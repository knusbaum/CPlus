#include <execinfo.h>
#include <stdlib.h>
#include <stdio.h>
//#include <cplus/exceptions.h>
//#include <cplus/concurrency.h>
#include "../headers/exceptions.h"

#define DOES_NOT_EXIST (void *)0x01

void function1();
void function2();
FILE * open_file(const char * filename, const char * mode);


void main()
{    


    // First Sample
    printf("Simple throw\n");
    try {
        printf("Throwing.\n");
        throw((void *)1);
    }
    catch(e, m) {
        printf("Caught %p\n", e);
    }


    // Sencond Sample
    printf("\nThrow from function with backtrace.\n");
    try {
        function1();
    }
    catch_bt(e, m) {
        printf("%s", e);
        print_backtrace(stdout);
        discard_backtrace();
    }


    // Third Sample (Nested)
    printf("\nNested try\n");
    try {
        try {
            printf("Throwing %p\n", (void *)0x1);
            throw((void *)0x1);
        }
        catch(e, m) {
            printf("Exception: %p\n", e);
            printf("Throwing %p\n", e + 1);
            throw(e + 1);
        }
    }
    catch(e, m) {
        printf("Exception: %p\n");
    }

    // Nested try in catch block
    printf("\nNested try in catch block\n");
    try {
        throw((void *)0x1);
    }
    catch (e, m) {

        try {
            throw((void *)0x1);
        }
        catch (ex, me) {
            printf("Caught!\n");
        }

    }

    //Showing sample open_file that throws exception.
    printf("\nSample open_file with exceptions\n");
    try {
        FILE * file = open_file("FakeFile.txt", "r");
    }
    catch(e, m) {
        printf("File does not exist!\n");
    }

    try {
        FILE * file = open_file("NewFile.txt", "w");
        printf("Opened file.\n");
        fclose(file);
    }
    catch(e, m) {
        printf("File does not exist!\n");
    }

    // Sample safe allocation
    try {
        void * x = tmalloc(10);
        tfree(x);
    }
    catch(e, m) {
    }

    // Sample free all of safely allocated memory.
    try {
        tmalloc(10);
        tmalloc(10);
        tmalloc(10);
        tmalloc(10);
        throw((void *)0x01);
    }
    catch(e, m) {
        tfreeall(m);
    }

    // Sample nested safe memory free. (within another try block)
    try {
        
        try {
            tmalloc(10);
            tmalloc(10);
            tmalloc(10);
            tmalloc(10);
            throw((void *)0x01);
        }
        catch(e, m) {
            tfreeall(m);
            throw(e);
        }
    }
    catch(e, m) {
        tfreeall(m);
    }
    
    // Sample nested safe memory free.
    try {
        
        try {
            tmalloc(10);
            tmalloc(10);
            tmalloc(10);
            tmalloc(10);
            throw((void *)0x01);
        }
        catch(e, m) {
            throw(e);
        }
    }
    catch(e, m) {
        tfreeall(m);
    }


    // Safely Allocated Memory Iteration Sample
    printf("\nSample safe memory iteration\n");
    try {
        int * x;
        x = tmalloc(sizeof(int));
        *x = 0xFF;
        x = tmalloc(sizeof(int));
        *x = 0xFE;
        x = tmalloc(sizeof(int));
        *x = 0xFD;
        throw((void *)0x01);
    }
    catch(e, m) {
        while(m != NULL)
        {
            int * x = tvalue(m);
            printf("Value of alloc'd memory: 0x%X\n", *x);
            m = tnext(m);
        }
        tfreeall(m);
    }

/**/
    printf("[main] Sucessfully exiting.\n");
}



void function1()
{
    function2();
}

void function2()
{
    throw("Wow!, Here's a value from function2!\n");   
}

FILE * open_file(const char * filename, const char * mode)
{
    FILE * file = fopen(filename, mode);
    if(file == NULL)
        throw(DOES_NOT_EXIST);
    
    return file;
}
