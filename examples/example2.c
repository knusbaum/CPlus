#include "../headers/exceptions.h"
#include "../headers/concurrency.h"

typedef struct function_data_s {
    channel * c;
    char * name;
} function_data;

void * con_fn(void * data);
void * die_fn(void * data);
void * mem_fn(void * data);
void * sleep_fn(void * data);

void main()
{    
    
    function_data process_dat;
    function_data thread_dat;
    
    process_dat.c = con_new_channel(sizeof(unsigned int), 10);
    process_dat.name = "process_fn";
    
    thread_dat.c = con_new_channel(sizeof(unsigned int), 10);
    thread_dat.name = "thread_fn";    
    
    printf("[main] Thread: %u\n", pthread_self());

    
    con_runnable * proc = con_run(con_fn, CON_PROC, &process_dat);
    con_runnable * thread = con_run(con_fn, CON_THREAD, &thread_dat);

    printf("Got process con_runnable %p\n", proc);
    printf("Got process con_runnable %p\n", thread);

    unsigned int x = 1;
    for(int i = 0; i < 5; i++)
    {
        printf("[main] Enqueueing %d\n", x);
        con_enqueue_channel(process_dat.c, &x);
        con_enqueue_channel(thread_dat.c, &x);
        x++;
        sleep(2);
    }
    printf("Finished enqueueing data.\n");

    printf("Waiting for concurrent actors to exit.\n");

    con_wait(proc);
    printf("[main] Process exited.\n");
    con_wait(thread);
    printf("[main] Thread exited.\n");


    con_destroy_channel(process_dat.c);
    con_destroy_channel(thread_dat.c);
 
    printf("\n[main] Trying Safe Memory in concurrent actors\n");

    proc = con_run(mem_fn, CON_PROC, NULL);
    thread = con_run(mem_fn, CON_THREAD, NULL);
    
    con_wait(proc);
    con_wait(thread);

    printf("\nStarting a thread pool of 2 tasks.\n");
    thread_pool * tp = thread_pool_init(2);
    int i;
    task * tasks[10];
    
    for(i = 0; i < 10; i++)
    {
        tasks[i] = thread_pool_queue_task(tp, sleep_fn, (void *)i);
    }

    printf("Finished queueing tasks. waiting for yeild.\n");
    
    for(i = 0; i < 10; i++)
    {
        thread_pool_yeild_task(tasks[i]);
    }
  

    printf("\n[main] Trying uncaught exceptions in concurrent actors\n");
    try {
        printf("[main] Launching die_fn process\n");
        proc = con_run(die_fn, CON_PROC, NULL);
        printf("[main] Launching die_fn thread\n");
        thread = con_run(die_fn, CON_THREAD, NULL);
        printf("sleeping 10.\n");
        sleep(10);
        printf("Done sleeping.\n");
    }
    catch(e,m) {
        printf("[main] Caught exception.\n");
    }
    
    printf("Waiting on proc and thread.\n");
    con_wait(proc);
    con_wait(thread);



    printf("[main] Sucessfully exiting.\n");

}

void * con_fn(void * data)
{
    function_data * d = data;

    printf("[%s] Thread: %u\n", d->name, pthread_self());

    for(int i = 0; i < 5; i++)
    {
        unsigned int x;
        con_dequeue_channel(d->c, &x);
        printf("[%s] Dequeued %d\n", d->name, x);
    }
    
    printf("[%s] read 5 elements. Waiting 5 seconds.\n", d->name);
    sleep(5);
    printf("[%s] exiting.\n", d->name);
    return NULL;
}

void * die_fn(void * data)
{
    printf("[die_fn] Beginning try block\n");
    try {
        throw((void *) 0x01);
    }
    catch(e,m) {
        printf("[die_fn] Caught!\n");
    }
    printf("[die_fn] Throwing uncaught exception.\n");
    throw(NULL);
}

void * mem_fn(void * data)
{
    int * x;
    try {
        x = tmalloc(10);
    }
    catch(e,m) {}

    tfree(x);
}

void * sleep_fn(void * data)
{
    printf("%d running!\n", ((int*)data));
    sleep(1);
}
