#include "../headers/refcount.h"
#include "../headers/concurrency.h"

void main()
{
    int * x = rmalloc(2 * sizeof(int));
    racquire(x);
    rrelease(x);
    x[1] = 10;
    x[2] = 20;
    
    rrelease(x);
    racquire(x);
}
