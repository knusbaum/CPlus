#include "mem_state.h"

pthread_mutex_t thread_stack_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t thread_stack_readers_mutex = PTHREAD_MUTEX_INITIALIZER;
unsigned int readers = 0;

thread_stack_node * head = NULL;

/**
 * @return the current thread_stack_node for this thread, or NULL;
 */
thread_stack_node * thread_stack()
{
    pthread_mutex_trylock(&thread_stack_mutex);
    pthread_mutex_lock(&thread_stack_readers_mutex);
    readers++;
    pthread_mutex_unlock(&thread_stack_readers_mutex);
    
    thread_stack_node * current = head;
    while(current != NULL)
    {
        if(current->thread_id == pthread_self())
            break;
        current = current->next;
    }

    pthread_mutex_lock(&thread_stack_readers_mutex);
    readers--;
    if(readers == 0)
    {
        pthread_mutex_unlock(&thread_stack_mutex);
    }
    pthread_mutex_unlock(&thread_stack_readers_mutex);
    
    return current;
}

/**
 * Destroy this thread's thread_stack_node.
 */
void thread_stack_destroy()
{
    pthread_mutex_lock(&thread_stack_mutex);
    thread_stack_node * current = head;
    
    if(current == NULL)
    {
        pthread_mutex_unlock(&thread_stack_mutex);
        return;
    }
    
    if(current->thread_id != pthread_self())
    {
        current = current->next;
        while(current != NULL)
        {
            if(current->thread_id == pthread_self())
                break;
        }
    }
    else
    {
        head = NULL;
    }
    
    if(current == NULL)
    {
        pthread_mutex_unlock(&thread_stack_mutex);
        return;
    }

    pthread_mutex_unlock(&thread_stack_mutex);

    state_stack_node * stack_node = current->top;
    while(stack_node != NULL)
    {
        //@todo look into recovery options for talloc'd memory.
        state_stack_node * next = stack_node->previous;
        free(stack_node);
        stack_node = next;
    }
    free(current);

}

/**
 * Destroy all thread_stack_nodes.
 */
void thread_stack_destroy_all()
{
    while(thread_stack() != NULL)
        thread_stack_destroy();
}

/**
 * Create a new thread stack for this thread.
 */
thread_stack_node * create_thread_stack()
{
    pthread_mutex_lock(&thread_stack_mutex);
    thread_stack_node * node = malloc(sizeof(thread_stack_node));
    node->next = head;
    head = node;
    node->thread_id = pthread_self();
    pthread_mutex_unlock(&thread_stack_mutex);
    node->top = NULL;
    node->except_throwval = NULL;
    node->last_backtrace.count = 0;
    node->last_backtrace.backtrace = NULL;

    return node;
}
