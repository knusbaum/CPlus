#ifndef STATE_STACK_H
#define STATE_STACK_H

#include "../headers/exceptions.h"
#include <inttypes.h>

#define REF 0
#define RAW 1

/**
 * Node for a linked-list structure holding memory allocated for a node
 */
typedef struct memnode_s {
    struct memnode_s * prev;
    struct memnode_s * next;
    uint32_t type;
} memnode;

/**
 * A stack node holding onto our register state.
 **/
typedef struct state_stack_node_s {
    jmp_buf regstate;
    memnode * memhead;
    memnode * memend;
    struct state_stack_node_s * previous;
} state_stack_node;

/**
 * A linked-list node to keep track of individual threads' catch states and safe memory.
 */
typedef struct thread_stack_node_s {
    pthread_t thread_id;
    struct thread_stack_node_s * next;
    state_stack_node * top;
    void * except_throwval;
    ex_backtrace last_backtrace;
} thread_stack_node;


/**
 * @return the current top of the state stack;
 */
state_stack_node * current_state();

/**
 * Migrates a memory list from one state to another. Used when a try block is exited.
 * @param from : the state containing the mlist we want to migrate
 * @param to : the state that we're going to add the mlist to
 */
void talloc_migrate_mem(state_stack_node * from, state_stack_node * to);

thread_stack_node * thread_stack();
void thread_stack_destroy();
thread_stack_node * create_thread_stack();


/**
 * Destroys all state stacks for all threads
 */
void thread_stack_destroy_all();



#endif
