#include "../headers/concurrency.h"
#include "../src/mem_state.h"

typedef struct channel_s {
    sem_t enqueue_sem;
    sem_t dequeue_sem;
    pthread_mutex_t enqueue_mutex;
    pthread_mutex_t dequeue_mutex;
    size_t head_index;
    size_t tail_index;
    size_t size;
    size_t count;
    char channel_data;
} channel;

typedef struct con_runnable_s {
    pthread_t thread;
    pid_t child;
    unsigned int mode;
} con_runnable;

con_runnable * con_run(runnable function, unsigned int mode, void * data)
{
    con_runnable * con = malloc(sizeof(con_runnable));
    con->mode = mode;
    if(mode == CON_THREAD)
    {
        pthread_create(&con->thread, NULL, function, data);
    }
    else if(mode == CON_PROC)
    {
        if((con->child = fork()) == 0)
        {
            thread_stack_destroy_all();
            free(con);
            (*function)(data);
            exit(0);
        }
    }
    return con;
}

void con_wait(con_runnable * con)
{
    if(con->mode == CON_THREAD)
    {
        pthread_join(con->thread, NULL);
    }
    else
    {
        unsigned int wait = 1;
        while(wait)
        {
            wait = 0;
            waitpid(con->child, NULL, 0);
            if(errno == EINTR)
                wait = 1;
        }
    }
    free(con);
}

channel * con_new_channel(size_t elem_size, size_t count)
{
    channel * c = mmap(NULL, sizeof(channel) + (elem_size * count), 
                       PROT_READ|PROT_WRITE, 
                       MAP_SHARED|MAP_ANONYMOUS, -1, 0);
    sem_init(&c->enqueue_sem, 0, count);
    sem_init(&c->dequeue_sem, 0, 0);
    pthread_mutex_init(&c->enqueue_mutex, NULL);
    pthread_mutex_init(&c->dequeue_mutex, NULL);
    c->head_index = 0;
    c->tail_index = 0;
    c->size = elem_size;
    c->count = count;
    return c;
}

void con_destroy_channel(channel * chan)
{
    pthread_mutex_destroy(&chan->enqueue_mutex);
    pthread_mutex_destroy(&chan->dequeue_mutex);
    sem_destroy(&chan->enqueue_sem);
    sem_destroy(&chan->dequeue_sem);
    munmap(chan, chan->size * chan->count);
}

void con_enqueue_channel(channel * chan, void * data)
{
    pthread_mutex_lock(&chan->enqueue_mutex);
    sem_wait(&chan->enqueue_sem);
    memcpy((&chan->channel_data) + (chan->head_index * chan->size), data, chan->size);
    chan->head_index++;
    pthread_mutex_unlock(&chan->enqueue_mutex);
    sem_post(&chan->dequeue_sem);
}

unsigned int con_enqueue_channel_noblock(channel * chan, void * data)
{
    unsigned int ret = 0;
    int val;
    pthread_mutex_lock(&chan->enqueue_mutex);
    sem_getvalue(&chan->enqueue_sem, &val);
    if(val > 0)
    {
        sem_wait(&chan->enqueue_sem);
        memcpy((&chan->channel_data) + (chan->tail_index * chan->size), data, chan->size);
        chan->head_index++;
        ret = 1;
    }
    pthread_mutex_unlock(&chan->enqueue_mutex);
    sem_post(&chan->dequeue_sem);

    return ret;
}

void con_dequeue_channel(channel * chan, void * data)
{
    pthread_mutex_lock(&chan->dequeue_mutex);
    sem_wait(&chan->dequeue_sem);
    memcpy(data, (&chan->channel_data) + (chan->tail_index * chan->size), chan->size);
    chan->tail_index++;
    pthread_mutex_unlock(&chan->dequeue_mutex);
    sem_post(&chan->enqueue_sem);
}

unsigned int con_dequeue_channel_noblock(channel * chan, void * data)
{
    unsigned int ret = 0;
    int val;
    pthread_mutex_lock(&chan->dequeue_mutex);
    sem_getvalue(&chan->dequeue_sem, &val);
    if(val > 0)
    {
        sem_wait(&chan->dequeue_sem);
        memcpy(data, (&chan->channel_data) + (chan->tail_index * chan->size), chan->size);
        chan->tail_index++;
        ret = 1;
    }
    pthread_mutex_unlock(&chan->dequeue_mutex);
    sem_post(&chan->enqueue_sem);
    
    return ret;
}
