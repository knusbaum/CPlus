#include "../headers/concurrency.h"

typedef struct task_s {
    //void * (*func)(void *);
    runnable func;
    void * dat;
    void * ret;
    uint16_t complete;
    uint16_t executing;
    uint16_t should_destroy;
    pthread_mutex_t complete_mutex;
    pthread_mutex_t task_mutex;
    struct task_s * next;
    struct task_s * prev;
} task;

typedef struct thread_pool_s {
    pthread_t * workers;
    task * task_queue;
    task * task_queue_tail;
    sem_t queue_sem;
    pthread_mutex_t queue_op_mutex;
} thread_pool;

void * thread_pool_worker(void * dat);
task * thread_pool_dequeue_task(thread_pool * tp);

thread_pool * thread_pool_init(size_t worker_count)
{
    thread_pool * new_pool = malloc(sizeof(thread_pool));
    new_pool->workers = calloc(worker_count, sizeof(pthread_t));
    new_pool->task_queue = NULL;
    new_pool->task_queue_tail = NULL;
    sem_init(&new_pool->queue_sem, 0, 0);
    pthread_mutex_init(&new_pool->queue_op_mutex, NULL);

    int i;
    for(i = 0; i < worker_count; i++)
    {
        pthread_create(&new_pool->workers[i], NULL, thread_pool_worker, new_pool);
    }
    return new_pool;
}

void * thread_pool_worker(void * dat)
{
    while(1)
    {
        thread_pool * pool = (thread_pool*)dat;
        sem_wait(&pool->queue_sem);
        task * mytask = thread_pool_dequeue_task(pool);
        if(mytask->should_destroy)
        {
            free(mytask);
        }
        else
        {
            mytask->executing = 1;
            pthread_mutex_unlock(&mytask->task_mutex);
            mytask->ret = mytask->func(dat);
            if(mytask->should_destroy)
            {
                free(mytask);
            }
            else
            {
                pthread_mutex_unlock(&mytask->complete_mutex);
                mytask->complete = 1;
                mytask->executing = 0;
            }
        }
    
    }    
}

task * thread_pool_dequeue_task(thread_pool * tp)
{
    task * task = NULL;
    pthread_mutex_lock(&tp->queue_op_mutex);
    if(tp->task_queue != NULL)
    {
      
        task = tp->task_queue;
        tp->task_queue = task->next;
        
        if(task->next == NULL)
            tp->task_queue_tail = NULL;
    }
    pthread_mutex_unlock(&tp->queue_op_mutex);
    pthread_mutex_lock(&task->task_mutex);
    task->prev = NULL;
    task->next = NULL;
    return task;
}

void * thread_pool_yeild_task(task * t)
{
    if(!t->complete)
    {
        pthread_mutex_lock(&t->complete_mutex);
        pthread_mutex_unlock(&t->complete_mutex);
    }
    return t->ret;
}

void thread_pool_destroy_task(task * t)
{
    
    pthread_mutex_lock(&t->task_mutex);
    if(t->next != NULL || t->prev != NULL || t->executing)
    {
        t->should_destroy = 1;
    }
    else
    {
        free(t);
        return;   
    }
    pthread_mutex_unlock(&t->task_mutex);
}

task * thread_pool_queue_task(thread_pool * tp, runnable fn, void * arg)
{
    task * t = malloc(sizeof(task));
    t->func = fn;
    t->dat = arg;
    t->ret = NULL;
    t->complete = 0;
    t->executing = 0;
    t->should_destroy = 0;
    pthread_mutex_init(&t->complete_mutex, NULL);
    pthread_mutex_init(&t->task_mutex, NULL);
    pthread_mutex_lock(&t->complete_mutex);

    pthread_mutex_lock(&(tp->queue_op_mutex));    
    if(tp->task_queue == NULL)
    {
        tp->task_queue = t;
        tp->task_queue_tail = t;
        t->next = NULL;
        t->prev = NULL;
    }
    else
    {
        tp->task_queue_tail->next = t;
        t->prev = tp->task_queue_tail;
        t->next = NULL;
        tp->task_queue_tail = t;
    }
    pthread_mutex_unlock(&tp->queue_op_mutex);
    
    sem_post(&tp->queue_sem);

    return t;
}
