#include "../headers/exceptions.h"
#include "mem_state.h"

#define MAX_BACKTRACE_LEN 1024


void * except_throwval = NULL;
ex_backtrace last_backtrace = {0, NULL};


/**
 * @return the current state node on the top of the stack. 
 */
state_stack_node * current_state()
{
    thread_stack_node * list = thread_stack();
    if(list == NULL) 
        return NULL;
    return list->top;
}

/**
 * @return the current state on the top of the stack, or die if none exists.
 */
void * except_current()
{
    state_stack_node * node = current_state();
    if(node == NULL)
    {
        fprintf(stderr, "0x%X UNCAUGHT_EXCEPTION\n", UNCAUGHT_EXCEPTION);
        exit(UNCAUGHT_EXCEPTION);
    }
    return &(node->regstate);
}

/**
 * Push a new state onto the state stack
 */
void except_state_push()
{
    state_stack_node * newnode = malloc(sizeof(state_stack_node));
    newnode->memhead = NULL;
    newnode->memend = NULL;

    thread_stack_node * stack = thread_stack();
    if(stack == NULL)
        stack = create_thread_stack();

    newnode->previous = stack->top;
    stack->top = newnode;
}

/**
 * Remove the state on the top of the stack.
 */
void except_state_pop()
{
    thread_stack_node * stack = thread_stack();
    state_stack_node * next = stack->top->previous;
    talloc_migrate_mem(stack->top, next);
    free(stack->top);
    stack->top = next;
    if(next == NULL)
    {
        except_throwval = stack->except_throwval;
        last_backtrace = stack->last_backtrace;
        thread_stack_destroy();
    }
}

/**
 * @return the thrown exception, or NULL if no exception has been thrown.
 * Clears the current exception.
 */
void * except_active()
{
    thread_stack_node * stack = thread_stack();
    void * active;
    if(stack != NULL)
    {
        active = stack->except_throwval;
        stack->except_throwval = NULL;
    }
    else
    {
        active = except_throwval;
        except_throwval = NULL;
    }
    return active;
}

/**
 * Throw an exception. Puts the exception pointer into the current exception,
 * and jumps to the state on the top of the stack.
 */
void throw(void * data)
{
    thread_stack_node * stack = thread_stack();
    jmp_buf * state = except_current();
        

    void * addr_trace[MAX_BACKTRACE_LEN];
    stack->last_backtrace.count = backtrace(addr_trace, MAX_BACKTRACE_LEN);
    stack->last_backtrace.backtrace = backtrace_symbols(addr_trace, 
                                                        stack->last_backtrace.count);
    
    stack->except_throwval = data;
    longjmp(except_current(), 1);
}

/**
 * Discard the backtrace if the user doesn't want it.
 */
void discard_backtrace()
{
    thread_stack_node * stack = thread_stack();
    if(stack == NULL)
    {
        if(last_backtrace.backtrace != NULL)
        {
            free(last_backtrace.backtrace);
            last_backtrace.backtrace == NULL;
        }
        last_backtrace.count = 0;
    }
    else
    {
        if(stack->last_backtrace.backtrace != NULL)
        {
            free(stack->last_backtrace.backtrace);
            stack->last_backtrace.backtrace = NULL;
        }
        stack->last_backtrace.count = 0;
    }
}


/**
 * Prints a backtrace to a file descriptor.
 * @param fd : the file descriptor to write the backtrace to
 */
void print_backtrace(FILE * fd)
{
    thread_stack_node * stack = thread_stack();
    if(stack == NULL)
    {
        for(int i = 0; i < last_backtrace.count; i++)
            fprintf(fd, "%s\n", last_backtrace.backtrace[i]);
    }
    else
    {
        for(int i = 0; i < stack->last_backtrace.count; i++)
            fprintf(fd, "%s\n", stack->last_backtrace.backtrace[i]);
    }
}
