#include "../headers/refcount.h"
#include "../headers/exceptions.h"

typedef struct ref_s {
    int references;
} ref;

void * rmalloc(size_t size)
{
    ref * data = malloc(size + sizeof(ref));
    data->references = 1;
    return ((char *)data) + sizeof(ref);
}

void * rcalloc(size_t size, size_t count)
{
    return rmalloc(size * count);
}

void racquire(void * ptr)
{
    ref * data = (ref*)(((char *)ptr) - sizeof(ref));
    if(data->references <= 0)
    {
        fprintf(stderr, "0x%X BAD_REF_ACQUIRE\n", BAD_REF_ACQUIRE);
        exit(BAD_REF_ACQUIRE);
    }
    data->references++;
    
}

void rrelease(void * ptr)
{
    ref * data = (ref*)(((char *)ptr) - sizeof(ref));
    data->references--;
    if(data->references < 0)
    {
        fprintf(stderr, "0x%X BAD_REF_RELEASE\n", BAD_REF_RELEASE);
        exit(BAD_REF_RELEASE);
    }
    
    if(data->references == 0)
        free(data);
}

int would_free(void * ptr)
{
    ref * data = (ref*)(((char *)ptr) - sizeof(ref));
    return (data->references == 0);
}

