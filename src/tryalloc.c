#include "../headers/tryalloc.h"
#include "../headers/refcount.h"
#include "mem_state.h"

memnode * caught_allocd = NULL;

state_stack_node * search_node_head_tail(memnode * node);
memnode * tmalloc_fn(size_t size, void * (*alloc_fn)(size_t));

void * tmalloc(size_t size) 
{
    memnode * newmem = tmalloc_fn(size, malloc);
    newmem->type = RAW;
    return ((char *)newmem) + sizeof(memnode);
}

void * trmalloc(size_t size)
{
    memnode * newmem = tmalloc_fn(size, rmalloc);
    newmem->type = REF;
    return ((char *)newmem) + sizeof(memnode);
}

memnode * tmalloc_fn(size_t size, void * (*alloc_fn)(size_t))
{
    state_stack_node * current = current_state();

    if(current == NULL)
    {
        fprintf(stderr, "0x%X NO_TRY_CONTEXT\n", NO_TRY_CONTEXT);
        exit(NO_TRY_CONTEXT);
    }
    
    memnode * newmem = alloc_fn(size + sizeof(memnode));
    newmem->next = NULL;

    if(current->memhead == NULL)
    {
        current->memhead = newmem;
        current->memend = newmem;
        newmem->prev = NULL;
    }
    else
    {
        newmem->prev = current->memend;
        current->memend->next = newmem;
        current->memend = newmem;
    }

    return newmem;
}

void trrelease(void * ptr)
{
    
    memnode * node = ((char *)ptr) - sizeof(memnode);
    int freed = would_free(node);
    if(freed)
    {
    
        if(caught_allocd == node)
            caught_allocd = node->next;

        state_stack_node * state_node = NULL;
        if(node->next == NULL)
        {
            state_node = search_node_head_tail(node);
            if(state_node != NULL)
                state_node->memend = node->prev;
        }
        else
        {
            node->next->prev = node->prev;
        }
        if(node->prev == NULL)
        {
            if(state_node == NULL)
                state_node = search_node_head_tail(node);
            if(state_node != NULL)
                state_node->memhead = node->next;
        }
        else
        {
            node->prev->next = node->next;
        }
    }
    rrelease(node);
}

void tracquire(void * ptr)
{
    memnode * node = ptr - sizeof(memnode);
    racquire(node);
}

void tfree(void * ptr)
{
    memnode * node = ptr - sizeof(memnode);

    if(caught_allocd == node)
        caught_allocd = node->next;

    state_stack_node * state_node = NULL;
    if(node->next == NULL)
    {
        state_node = search_node_head_tail(node);
        if(state_node != NULL)
            state_node->memend = node->prev;
    }
    else
    {
        node->next->prev = node->prev;
    }
    if(node->prev == NULL)
    {
        if(state_node == NULL)
            state_node = search_node_head_tail(node);
        if(state_node != NULL)
            state_node->memhead = node->next;
    }
    else
    {
        node->prev->next = node->next;
    }

    free(node);
}

void tfreeall(void * mlist)
{
    memnode * current = (memnode*)mlist;
    while(current != NULL)
    {
        memnode * temp = current->next;
        if(current->type == RAW)
            tfree(((char *)current) + sizeof(memnode));
        else if(current->type == REF)
            trrelease(((char *)current) + sizeof(memnode));
        current = temp;
    }
}

void * tvalue(void * mlist)
{
    return ((char *)mlist) + sizeof(memnode);
}

void * tnext(void * mlist)
{
    return ((memnode *)mlist)->next;
}

/**
 * @return a pointer to the memory list of the most recently popped stack state.
 */
void * tallocd()
{
    return caught_allocd;
}

void talloc_migrate_mem(state_stack_node * from, state_stack_node * to)
{
    if(to != NULL)
    {
        if(to->memhead == NULL)
        {
            to->memhead = from->memhead;
            to->memend = from->memend;
        }
        else
        {
            to->memend->next = from->memhead;
            to->memend = from->memend;
        }
    }
    
    caught_allocd = from->memhead;
    from->memhead = NULL;
    from->memend = NULL;
}

state_stack_node * search_node_head_tail(memnode * node)
{
    state_stack_node * current = current_state();
    while(current != NULL)
    {
        if(current->memhead == node || current->memend == node)
            break;
        
        current = current->previous;
    }
    return current;
}
