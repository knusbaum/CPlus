#include <check.h>
#include "../headers/concurrency.h"

START_TEST (channel_enqueue_dequeue)
{
    channel * c = con_new_channel(sizeof(unsigned int), 10);
    
    for(unsigned int i = 0; i < 10; i++)
    {
        con_enqueue_channel(c, &i);
    }

    for(unsigned int i = 0; i < 10; i++)
    {
        unsigned int x;
        con_dequeue_channel(c, &x);
        ck_assert_int_eq(x, i);
    }
}
END_TEST

#define TEST_VAL 10

START_TEST (channel_enqueue_noblock)
{
    channel * c = con_new_channel(sizeof(unsigned int), TEST_VAL);
    
    unsigned int ret;
    for(int i = 0; i < TEST_VAL; i++)
    {
        ret = con_enqueue_channel_noblock(c, &i);
        ck_assert_int_eq(ret, CHAN_SUCCESS);
    }
    
    unsigned int x = TEST_VAL;
    ret = con_enqueue_channel_noblock(c, &x);
    
    ck_assert_int_eq(ret, CHAN_WOULDBLOCK);

}
END_TEST

START_TEST (channel_dequeue_noblock)
{
    channel * c = con_new_channel(sizeof(unsigned int), 10);
    unsigned int x = 0;
    unsigned int ret = con_dequeue_channel_noblock(c, &x);
    ck_assert_int_eq(ret, CHAN_WOULDBLOCK);
    
    unsigned int y = TEST_VAL;
    con_enqueue_channel(c, &y);
    ret = con_dequeue_channel_noblock(c, &x);

    ck_assert_int_eq(ret, CHAN_SUCCESS);
    ck_assert_int_eq(x, TEST_VAL);
}
END_TEST

void * test_fn(void * chan)
{

    unsigned int x = TEST_VAL;
    channel * c = chan;
    con_enqueue_channel(c, &x);
    return NULL;
}

START_TEST (concurrent_thread)
{
    channel * c = con_new_channel(sizeof(unsigned int), TEST_VAL);
    unsigned int x;
    con_run(test_fn, CON_THREAD, c);
    con_dequeue_channel(c, &x);
    ck_assert_int_eq(x, TEST_VAL);
}
END_TEST

START_TEST(concurrent_process)
{
    channel * c = con_new_channel(sizeof(unsigned int), TEST_VAL);
    unsigned int x;
    con_run(test_fn, CON_PROC, c);
    con_dequeue_channel(c, &x);
    ck_assert_int_eq(x, TEST_VAL);
}
END_TEST

void * wait_fn(void * chan)
{
    channel * c = chan;
    sleep(1);
    unsigned int x = TEST_VAL;
    con_enqueue_channel(c, &x);
    return NULL;
}

START_TEST(concurrent_wait_thread)
{
    channel * c = con_new_channel(sizeof(unsigned int), TEST_VAL);
    con_runnable * thread = con_run(wait_fn, CON_THREAD, c);
    
    unsigned int x = 0;
    unsigned int ret;
    ret = con_dequeue_channel_noblock(c, &x);
    ck_assert_int_eq(ret, CHAN_WOULDBLOCK);
    
    con_wait(thread);
    ret = con_dequeue_channel_noblock(c, &x);
    ck_assert_int_eq(ret, CHAN_SUCCESS);
    ck_assert_int_eq(x, TEST_VAL);
}
END_TEST

START_TEST(concurrent_wait_process)
{
    channel * c = con_new_channel(sizeof(unsigned int), TEST_VAL);
    con_runnable * thread = con_run(wait_fn, CON_PROC, c);
    
    unsigned int x = 0;
    unsigned int ret;
    ret = con_dequeue_channel_noblock(c, &x);
    ck_assert_int_eq(ret, CHAN_WOULDBLOCK);
    
    con_wait(thread);
    ret = con_dequeue_channel_noblock(c, &x);
    ck_assert_int_eq(ret, CHAN_SUCCESS);
    ck_assert_int_eq(x, TEST_VAL);
}
END_TEST
