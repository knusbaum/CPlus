#include "../headers/exceptions.h"
#include "../headers/tryalloc.h"

int main()
{
    try {
        void * w = tmalloc(10);
        void * x = tmalloc(10);
        void * y = tmalloc(10);
        void * z = tmalloc(10);
        tfree(w);
        tfree(x);
        tfree(y);
        tfree(z);
    }
    catch(e, m) {}
    
    return 0;
}
