#include <check.h>
#include "../headers/exceptions.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define DUMMY_PTR (void *)0x10

START_TEST (catch_only_throws)
{
    void * val = NULL;
    try{
    }
    catch(e, m) {
        val = DUMMY_PTR;
    }

    ck_assert_ptr_eq(val, NULL);
}
END_TEST

START_TEST (throw_halts_try)
{
    void * val = NULL;
    try {
        throw(NULL);
        val = DUMMY_PTR;
    }
    catch(e, m) {
    }
    ck_assert_ptr_eq(val, NULL);
}
END_TEST

START_TEST (catch_catches)
{
    void * val = DUMMY_PTR;
    try {
        throw((void *)1);
    }
    catch(e, m) {
        val = NULL;
    }

    ck_assert_ptr_eq(val, NULL);
}
END_TEST

START_TEST(catch_catches_thrown_val)
{
    void * val = NULL;
    try {
        throw(DUMMY_PTR);
    }
    catch(e, m) {
        val = e;
    }
    ck_assert_ptr_eq(val, DUMMY_PTR);
}
END_TEST

START_TEST(nested_try)
{
    void * val1 = NULL;
    void * val2 = NULL;
    try {
        try {
            throw(DUMMY_PTR);
        }
        catch(e, m) {
            val1 = e;
            throw(e + 1);
        }
    }
    catch(e, m) {
        val2 = e;
    }
    ck_assert_ptr_eq(val1, DUMMY_PTR);
    ck_assert_ptr_eq(val2, DUMMY_PTR + 1);

}
END_TEST

void recursive_thrower(int x)
{
    if(x > 0)
        recursive_thrower(x - 1);
    else
        throw(DUMMY_PTR);
}

START_TEST(try_functions)
{
    void * val = NULL;
    try {
        recursive_thrower(10);
    }
    catch(e, m) {
        val = e;
    }

    ck_assert_ptr_eq(val, DUMMY_PTR);
}
END_TEST

START_TEST(uncaught_exception)
{
    if(fork())
    {
        int x;
        printf("uncaught_exception\n");
        wait(&x);
        ck_assert_int_eq(WEXITSTATUS(x), UNCAUGHT_EXCEPTION);
    }
    else
    {
        throw(NULL);
    }
    
}
END_TEST

START_TEST (exceptions_backtrace)
{
    printf("\033[1;32mexceptions_backtrace\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 -q ./exceptions_backtrace");
    
    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST
