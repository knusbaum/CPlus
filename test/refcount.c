#include <check.h>
#include "../headers/refcount.h"
#include <sys/types.h>


START_TEST (refcount_ralloc)
{
    int * x = rmalloc(2 * sizeof(int));
    x[0] = 10;
    x[1] = 20;
    ck_assert_int_eq(x[0], 10);
    ck_assert_int_eq(x[1], 20);
}
END_TEST

START_TEST (refcount_rrelease)
{
    printf("\033[1;32mrefcount_rrelease\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 --track-origins=yes -q ./refcount_rrelease");

    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST (refcount_racquire)
{
    printf("\033[1;32mrefcount_racquire\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 --track-origins=yes -q ./refcount_racquire");

    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST (refcount_freeall)
{
    printf("\033[1;32mrefcount_freeall\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 --track-origins=yes -q ./refcount_freeall");

    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST (refcount_freeall_safe)
{
    printf("\033[1;32mrefcount_freeall_safe\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 --track-origins=yes -q ./refcount_freeall_safe");

    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST(refcount_bad_acquire)
{
    if(fork())
    {
        int x;
        wait(&x);
        ck_assert_int_eq(WEXITSTATUS(x), BAD_REF_ACQUIRE);
    }
    else
    {
        void * x = rmalloc(2 * sizeof(int));
        rrelease(x);
        racquire(x);
    }
    
}
END_TEST

START_TEST(refcount_bad_release)
{
    if(fork())
    {
        int x;
        wait(&x);
        ck_assert_int_eq(WEXITSTATUS(x), BAD_REF_RELEASE);
    }
    else
    {
        void * x = rmalloc(2 * sizeof(int));
        rrelease(x);
        rrelease(x);
    }
    
}
END_TEST
