#include "../headers/exceptions.h"

void function1();
void function2();

int main()
{
    void * allocs[10];
    try {
        function1();
    }
    catch_bt(e, m) {
        print_backtrace(stdout);
        discard_backtrace();
    }
    
    return 0;
}

void function1()
{
    function2();
}

void function2()
{
    throw((void *)0x01);
}
