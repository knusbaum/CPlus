#include "../headers/exceptions.h"
#include "../headers/tryalloc.h"

int main()
{
    try {
        try {
            tmalloc(10);
            tmalloc(10);
            tmalloc(10);
            tmalloc(10);
            throw((void *)0x01);
        }
        catch(e, m) {
            throw((void *) 0x01);
        }
    }
    catch(e, m) {
        tfreeall(m);
    }

    return 0;
}
