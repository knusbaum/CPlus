#include "../headers/refcount.h"

int main()
{
    int * x = rmalloc(2 * sizeof(int));
    x[0] = 10;
    x[1] = 20;
    rrelease(x);
    return 0;
}
