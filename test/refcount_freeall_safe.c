#include "../headers/refcount.h"
#include "../headers/exceptions.h"
#include "../headers/tryalloc.h"

int main()
{
    void * x;
    try {
        x = trmalloc(2 * sizeof(int));
        trmalloc(2 * sizeof(int));
        tracquire(x);
        throw((void *)0x01);
    }
    catch (e, m) {
        tfreeall(m);
    }
    
    trrelease(x);

    return 0;
}

