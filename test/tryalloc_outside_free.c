#include "../headers/exceptions.h"
#include "../headers/tryalloc.h"

int main()
{
    void * x;
    try {
        x = tmalloc(10);
    }
    catch(e, m) {
    }

    tfree(x);

    return 0;
}
