#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include "exceptions.c"
#include "tryalloc.c"
#include "concurrency.c"
#include "refcount.c"

Suite * Exceptions_suite()
{
    Suite *s = suite_create("Exceptions");
    
    /* Exceptions Unit Tests */
    TCase *tc_exceptions = tcase_create("Exceptions");
    tcase_add_test(tc_exceptions, catch_catches);
    tcase_add_test(tc_exceptions, catch_only_throws);
    tcase_add_test(tc_exceptions, throw_halts_try);
    tcase_add_test(tc_exceptions, catch_catches_thrown_val);
    tcase_add_test(tc_exceptions, nested_try);
    tcase_add_test(tc_exceptions, try_functions);
    tcase_add_test(tc_exceptions, uncaught_exception);
    tcase_add_test(tc_exceptions, exceptions_backtrace);
    suite_add_tcase(s, tc_exceptions);

    return s;
}

Suite * Safe_Memory_suite()
{
    Suite *s = suite_create("Safe Memory");

    /* Safe Memory Allocation Tests */
    TCase *tc_safe_memory = tcase_create("Safe Memory");
    tcase_add_test(tc_safe_memory, tryalloc_simple_free);
    tcase_add_test(tc_safe_memory, tryalloc_outside_free);
    tcase_add_test(tc_safe_memory, tryalloc_catch_freeall);
    tcase_add_test(tc_safe_memory, tryalloc_nested_freeall);
    tcase_add_test(tc_safe_memory, tryalloc_nested_deep_freeall);
    tcase_add_test(tc_safe_memory, tryalloc_miscellaneous);
    tcase_add_test(tc_safe_memory, tryalloc_iterate_memory);
    suite_add_tcase(s, tc_safe_memory);

    return s;
}

Suite * Concurrency_suite()
{
    Suite *s = suite_create("Concurrency");
    
    /* Concurrency Tests */
    TCase *tc_concurrency = tcase_create("Concurrent Tests");
    tcase_add_test(tc_concurrency, channel_enqueue_dequeue);
    tcase_add_test(tc_concurrency, channel_enqueue_noblock);
    tcase_add_test(tc_concurrency, channel_dequeue_noblock);
    tcase_add_test(tc_concurrency, concurrent_thread);
    tcase_add_test(tc_concurrency, concurrent_process);
    tcase_add_test(tc_concurrency, concurrent_wait_thread);
    tcase_add_test(tc_concurrency, concurrent_wait_process);
    suite_add_tcase(s, tc_concurrency);
    
    return s;
}

Suite * Refcount_suite()
{
    Suite *s = suite_create("Refcount");
    
    /* Concurrency Tests */
    TCase *tc_refcount = tcase_create("Refcount Tests");
    tcase_add_test(tc_refcount, refcount_ralloc);
    tcase_add_test(tc_refcount, refcount_rrelease);
    tcase_add_test(tc_refcount, refcount_racquire);
    tcase_add_test(tc_refcount, refcount_freeall);
    tcase_add_test(tc_refcount, refcount_freeall_safe);
    tcase_add_test(tc_refcount, refcount_bad_acquire);
    tcase_add_test(tc_refcount, refcount_bad_release);
    suite_add_tcase(s, tc_refcount);
    
    return s;
}

int main(void)
{
    int number_failed;
    
    Suite *s = Exceptions_suite();
    SRunner *runner = srunner_create(s);
    srunner_run_all(runner, CK_VERBOSE);
    number_failed = srunner_ntests_failed(runner);
    srunner_free(runner);
    
    s = Safe_Memory_suite();
    runner = srunner_create(s);
    srunner_run_all(runner, CK_VERBOSE);
    number_failed += srunner_ntests_failed(runner);
    srunner_free(runner);

    s = Concurrency_suite();
    runner = srunner_create(s);
    srunner_run_all(runner, CK_VERBOSE);
    number_failed += srunner_ntests_failed(runner);
    srunner_free(runner);

    s = Refcount_suite();
    runner = srunner_create(s);
    srunner_run_all(runner, CK_VERBOSE);
    number_failed += srunner_ntests_failed(runner);
    srunner_free(runner);
    
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}


