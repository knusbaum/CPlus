#include "../headers/exceptions.h"
#include "../headers/tryalloc.h"

void function1();
void function2();

int main()
{
    void * allocs[10];
    try {
        try {
            allocs[0] = tmalloc(10);
            allocs[1] = tmalloc(10);
            allocs[2] = tmalloc(10);

            tfree(allocs[2]);
            
            throw((void *)0x01);
        }
        catch(e, m) {

            tfree(allocs[1]);
            for(int i = 3; i < 10; i++)
                allocs[i] = tmalloc(10);
            
            tfree(allocs[5]);
            function1();
            
            throw((void *)0x01);
        }
    }
    catch(e, m) {
        tfreeall(m);
    }

    return 0;
}


void function1()
{
    function2();
}

void function2()
{
    tmalloc(10);
}
