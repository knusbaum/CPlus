#include <check.h>
#include "../headers/exceptions.h"
#include "../headers/tryalloc.h"

START_TEST (tryalloc_simple_free)
{
    printf("\033[1;32mtryalloc_simple_free\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 --track-origins=yes -q ./tryalloc_simple_free");

    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST (tryalloc_outside_free)
{
    printf("\033[1;32mtryalloc_outside_free\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 -q ./tryalloc_outside_free");

    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST (tryalloc_catch_freeall)
{
    printf("\033[1;32mtryalloc_catch_freeall\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 -q ./tryalloc_catch_freeall");
    
    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST (tryalloc_nested_freeall)
{
    printf("\033[1;32mtryalloc_nested_freeall\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 -q ./tryalloc_nested_freeall");
    
    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST (tryalloc_nested_deep_freeall)
{
    printf("\033[1;32mtryalloc_nested_deep_freeall\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 -q ./tryalloc_nested_deep_freeall");
    
    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST (tryalloc_miscellaneous)
{
    printf("\033[1;32mtryalloc_miscellaneous\033[0m\n");
    int ret = system("valgrind --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --error-exitcode=1 -q ./tryalloc_miscellaneous");
    
    ret = WEXITSTATUS(ret);
    ck_assert_int_eq(ret, 0);
        
}
END_TEST

START_TEST (tryalloc_iterate_memory)
{
    void * allocs[10];
    try {
        for(int i = 0; i < 10; i++)
            allocs[i] = tmalloc(10);
        throw((void *)0x01);
    }
    catch(e, m) {
        int i = 0;
        while(m != NULL)
        {
            ck_assert_ptr_eq(tvalue(m), allocs[i]);
            i++;
            m = tnext(m);
        }
    }
    
}
END_TEST
