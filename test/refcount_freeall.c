#include "../headers/refcount.h"
#include "../headers/exceptions.h"
#include "../headers/tryalloc.h"

int main()
{
    try {
        void * x = trmalloc(2 * sizeof(int));
        trmalloc(2 * sizeof(int));
        throw((void *)0x01);
    }
    catch (e, m) {
        tfreeall(m);
    }

    return 0;
}

