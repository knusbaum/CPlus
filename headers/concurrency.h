#ifndef CONCURRENCY_H
#define CONCURRENCY_H

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <memory.h>
#include <inttypes.h>

#define CON_PROC 0
#define CON_THREAD 1

#define CHAN_WOULDBLOCK 0
#define CHAN_SUCCESS 1

typedef struct channel_s channel; 
typedef struct con_runnable_s con_runnable;
typedef void * (*runnable)(void *);
typedef struct task_s task;
typedef struct thread_pool_s thread_pool;

con_runnable * con_run(runnable function, unsigned int mode, void * data); 
void con_wait(con_runnable * con);

channel * con_new_channel(size_t elem_size, size_t count);
void con_destroy_channel(channel * chan);

void con_enqueue_channel(channel * chan, void * data);
unsigned int con_enqueue_channel_noblock(channel * chan, void * data);
void con_dequeue_channel(channel * chan, void * data);
unsigned int con_dequeue_channel_noblock(channel * chan, void * data);

thread_pool * thread_pool_init(size_t worker_count);
task * thread_pool_queue_task(thread_pool * tp, runnable fn, void * arg);
void thread_pool_destroy_task(task * t);
void * thread_pool_yeild_task(task * t);

#endif
