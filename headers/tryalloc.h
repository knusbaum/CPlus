#ifndef TRYALLOC_H
#define TRYALLOC_H

#include <stdlib.h>
#include <stdio.h>

/**
 * Used to allocate memory in an error-prone section of code.
 * Memory allocated with this function can be manipulated in catch blocks
 * after an exception is thrown.
 * @param size : amount of memory in bytes to allocate.
 * @return a pointer to the allocated block
 */
void * tmalloc(size_t size);
void * trmalloc(size_t size);

/**
 * Used to free memory allocated with tmalloc. All memory allocated with tmalloc should be
 * freed with tfree, and only memory allocated with tmalloc should be freed with tfree.
 * @param ptr : pointer to the block of memory to free
 */
void tfree(void * ptr);
void trrelease(void * ptr);

void tracquire(void * ptr);

/**
 * Should be used only within catch blocks to free all the memory that's been allocated
 * in any functions executed within the corresponding try block.
 * Also releases any memory acquired with trmalloc.
 * @param mlist : the mlist given by the catch block
 */
void tfreeall(void * mlist);

/**
 * Used to extract the current void * value from an mlist. Use this to iterate an mlist
 * given by a catch statement.
 * @param mlist : the mlist given by the catch statement.
 * @return the void * that this mlist entry corresponds to
 */
void * tvalue(void * mlist);

/**
 * Used to iterate an mlist given by a catch statement.
 * @param mlist : the mlist given by the catch statement
 * @return an mlist where the current entry is the next entry in the passed mlist.
 */
void * tnext(void * mlist);

/* Internal Use */
void * tallocd();

#endif
