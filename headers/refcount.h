#ifndef REFCOUNT_H
#define REFCOUNT_H

#include <stdlib.h>
#include <stdio.h>

void * rmalloc(size_t size);
void * rcalloc(size_t size, size_t count);
void racquire(void * ptr);
void rrelease(void * ptr);


#endif
