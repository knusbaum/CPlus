#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <execinfo.h>
#include <setjmp.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "tryalloc.h"

typedef struct ex_backtrace_s {
    size_t count;
    char ** backtrace;
} ex_backtrace;

/* Print a backtrace provided by a catch statement */
void print_backtrace(FILE * fd);

/* Free a backtrace provided by a catch statement */
void discard_backtrace();

/* Throw an exception */
void throw(void * data);

#define try except_state_push(); if(!setjmp(except_current()))
#define catch(E,M) except_state_pop(); discard_backtrace(); for(void * E = except_active(), * M = tallocd(); E != NULL; E = NULL)
#define catch_bt(E,M) except_state_pop(); for(void * E = except_active(), * M = tallocd(); E != NULL; E = NULL)

#define UNCAUGHT_EXCEPTION 0xFF
#define NO_TRY_CONTEXT 0xFE
#define BAD_FREE 0xFD
#define BAD_REF_ACQUIRE 0xFC
#define BAD_REF_RELEASE 0xFB


/**
 * Usage (prints "Caught 0x1"):

    try {
        printf("Throwing.\n");
        throw((void *)1);
    }
    catch(e, m) {
        printf("Caught %p\n", e);
    }


 * Expands to:

    except_state_push();              //Push a new state onto the state stack
    if (!setjmp(except_current()))    //Write to the current state
    {
        printf("Throwing.\n");        //Execute try body
        throw((void *)1);             //Throw an exception
    }
    except_state_pop();               //Remove the current state (we're done with it)
    //If an exception was thrown, handle it.
    for(void * e = except_active(), * m = tallocd(); e != NULL; e = NULL) 
    {
        printf("Caught %p\n", e);
    }
**/

/* Internal Use */
void * except_current();
void except_state_push();
void except_state_pop();
void * except_active();


#endif
